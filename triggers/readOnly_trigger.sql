delimiter $$
create trigger StopInsertValuePost
before insert
on post for each row
begin
   signal sqlstate '45000' set message_text = 'Inserting error';
end;$$

create trigger StopUpdateValuePost
before update 
on post for each row
begin 
   signal sqlstate '45000' set message_text = 'Updating error';
end;$$

create trigger StopDeleteValuePost
before delete 
on post for each row
begin
   signal sqlstate '45000' set message_text = 'Deleting error';
end;$$
delimiter ;