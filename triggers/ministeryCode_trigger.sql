delimiter $$
create trigger BeforeInsertMinistryCode
before insert 
on medicine for each row
begin
	if new.ministry_code not rlike '^[А-Я&&[^МП]]{2}-\d{3}-\d{2}' 
    then signal sqlstate '45000' set message_text = 'Error!!!';
    end if;
end $$
delimiter ;

delimiter $$
create trigger BeforeUpdateMinistryCode
before update 
on medicine for each row
begin
	if new.ministry_code not rlike '^[А-Я&&[^МП]]{2}-\d{3}-\d{2}'  
    then signal sqlstate '45000' set message_text = 'Error!!!';
    end if;
end $$
delimiter ;