delimiter $$
create trigger BeforeInsertEmployee 
	before insert on employee
	for each row
begin
	if new.post != all((select post from post)) or new.pharmacy_id != all((select id from pharmacy))
	then 
	signal sqlstate '45000'
	set message_text = 'Insertion Error! Foreign keys does not match!' ;
	end if;
end$$
delimiter ;

-- before update
delimiter $$
create trigger BeforeUpdateEmployee
	before update on employee
	for each row
begin
 	if new.pharmacy_id != all((select id from pharmacy)) or new.post != all((select post from post))
 	then
 	signal sqlstate '45000'
 	set message_text = 'Update Error!';
 	end if;
end $$
delimiter ;


# post table 
-- before delete
delimiter $$
create trigger BeforeDeletePost
	before delete on post
	for each row
begin
	if old.post = any((select post from employee))
	then 
	signal sqlstate '45000'
	set message_text = 'Delete Error!';	
	end if;
end $$
delimiter ;


# pharmacy table 
-- before insert
delimiter $$
create trigger BeforeInsertPharmacy
	before insert on pharmacy
	for each row
begin
	if new.street != all((select street from street))
	then 
	signal sqlstate '45000'
	set message_text = 'Insertion Error! Foreign keys does not match!';
	end if;
end $$
delimiter ;

-- before update
delimiter $$
create trigger BeforeUpdatePharmacy
	before update on pharmacy
	for each row
begin
	if new.street != any((select street from street))
	then
	signal sqlstate '45000'
	set message_text = 'Update Error!';
	end if;
end $$
delimiter ;

-- before delete
delimiter $$
create trigger BeforeDeletePharmacy
	before delete on pharmacy
	for each row
begin
	if old.id = any((select pharmacy_id from employee)) or old.id = any((select pharmacy_id from pharmacy_medicine))
	then
	signal sqlstate '45000'
	set message_text = 'Delete Error!';
	end if;
end $$
delimiter ;


# street table 
-- before delete
delimiter $$
create trigger BeforeDeleteStreet
	before delete on street
	for each row
begin
	if old.street = any((select street from pharmacy))
	then
	signal sqlstate '45000'
	set message_text = 'Delete Error!'; 
	end if;
end $$
delimiter ;


# medicine table 
-- before delete
delimiter $$
create trigger BeforeDeleteMedicine
	before delete on medicine
	for each row
begin
	if old.id = any((select medicine_id from medicine_zone)) or old.id = any((select medicine_id from pharmacy_medicine))
	then
	signal sqlstate '45000'
	set message_text = 'Delete Error!';
	end if;
end $$
delimiter ;


# pharmacy_medicine 


-- before update
delimiter $$
create trigger BeforeUpdatePharmacyMedicine
before update on pharmacy_medicine
for each row
begin
	if new.pharmacy_id != any((select id from pharmacy)) or new.medicine_id != any((select id from medicine))
	then
	signal sqlstate '45000'
	set message_text = 'Update Error!';
	end if;
end $$
delimiter ;


# zone
-- before delete
delimiter $$
create trigger BeforeDeleteZone
	before delete on zone
	for each row
begin
	if old.id = any((select zone_id from medicine_zone))
	then
	signal sqlstate '45000'
	set message_text = 'Delete Error!';
	end if;
end $$
delimiter ;


# medicine_zone 
-- before insert
delimiter $$
create trigger BeforeInsertMedicineZone
	before insert on medicine_zone
	for each row
begin 
	if new.medicine_id != all((select id from medicine)) or new.zone_id != all((select id from zone))
	then 
	signal sqlstate '45000'
	set message_text = 'Insertion Error! Foreign keys does not match!';
	end if;
end $$
delimiter ;

-- before update
delimiter $$
create trigger BeforeUpdateMedicineZone
	before update on medicine_zone
	for each row
begin
if new.medicine_id != any((select id from medicine)) or new.zone_id != any((select id from zone))
	then
	signal sqlstate '45000'
	set message_text = 'Update Error!';
	end if;
end $$
delimiter ;