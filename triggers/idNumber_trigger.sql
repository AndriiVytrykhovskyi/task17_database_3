delimiter $$
create trigger BeforeUpdateIdEmployeeCheck
before insert 
on employee for each row
begin
	if new.identity_number rlike '0{2}$' 
    then SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Bad format. Id can`t end with 00';
    end if;
end $$
delimiter ;

delimiter $$
create trigger BeforeInsertIdEmployeeCheck
before insert 
on employee for each row
begin
	if new.identity_number rlike '0{2}$' 
    then SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Bad format. Id can`t end with 00';
    end if;
end $$
delimiter ;

