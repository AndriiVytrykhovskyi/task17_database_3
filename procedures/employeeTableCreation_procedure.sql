delimiter $$
create procedure employeeTableCreation()
begin
declare done int default false;
	declare emplName varchar(50);
	declare EmplCursor 	
	cursor for select name from employee;
	declare continue handler
	for not found set done = true;
	
	open emplCursor;
	get_empl: loop
	fetch emplCursor into emplName;
	if done = true
	then leave get_empl;
	end if;
	set @table = concat('CREATE TABLE ', emplName, '( id int primary key auto_increment );');

	prepare myquery from @table;
	execute myquery;
	deallocate prepare myquery;
	end loop;

	close EmplCursor; 
end $$
delimiter ;