delimiter $$
create procedure addEmployee(in surname varchar(30), 
				in name char(30), 
				in midle_name varchar(30), 
				in identity_number char(10),
				in passport char(10),
				in experience decimal(10,1),
				in birthday date,
				in post varchar(15),
				in pharmacy_id int(11))
begin
	if post != all((select post from post)) or pharmacy_id != all((select id from pharmacy))
	then
	signal sqlstate '45000'
	set message_text = 'Insertion Error! Foreign keys does not match!';
	else
	insert into employee(surname, name, midle_name, identity_number, passport, experience, birthday, post, pharmacy_id)
		values(surname, name, midle_name, identity_number, passport, experience, birthday, post, pharmacy_id);
	end if;
end $$
delimiter ;
