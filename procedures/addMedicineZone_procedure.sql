delimiter $$
create procedure addMedicineZone(in medicine_id int, in zone_id int)
begin
	if medicine_id = any((select id from medicine)) and zone_id = any((select id from zone))
	then
	insert into medicine_zone
		values(medicine_id, zone_id);
	else
	signal sqlstate '45000'
	set message_text = "Insertion Error! Foreign keys does not match!";
	end if;
end $$
delimiter ;