delimiter $$
create function getFullPharmacyAdress(p_id int) returns varchar(500)
reads sql data
deterministic
begin
	declare fullAddress varchar(50);
	declare pharmacy_name varchar(50);
	declare build_number int;
	select  name, building_number into pharmacy_name, build_number from pharmacy where id = p_id;
	set fullAddress = concat(pharmacy_name, ' ', build_number);
	return fullAddress;
end $$
delimiter ;