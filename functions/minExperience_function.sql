delimiter $$
create function getMinExperienceFromEmployee() returns decimal(10,1)
reads sql data
deterministic
begin
	declare min_experience decimal(10, 1);
 	select MIN(experience) from employee into min_experience;
	return min_experience;
end $$
delimiter ;